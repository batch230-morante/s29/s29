// Advance Query Operators
	// We want more flexible querying of data within MongoDB

// [SECTION] Comparison Query Operators

// $gt/$gte operator 
/*
- Allows us to find documents that have field number values greater than or equal to specified value.
- Syntax:
			db.collectionName.find({field:{$gt/$gte: value}});
*/

	
	// $gt - greater than
	// $gte - greater than or equal 


	db.users.find(
		{
			age:{$gt: 21}
		}
	)

	db.users.find(
		{
			age:{$gte: 21}
		}
	)

// $lt/$lte operator
/*
	- Allows us to find documents that have the field number values less than or equal to a specified value.
	- Syntax:
		db.collectionName.find({field: {$lt/$lte: value}});
	
*/

	// $lt - less than
	// $lte - less than or equal

	db.users.find(
		{
			age: {
				$lt: 82
			}
		}
	)

	db.users.find(
		{
			age: {
				$lte: 82
			}
		}
	)


	 // $ne operators
	/*
	- Allows us to find documents field number values not equal to a specified value.
	-Syntax:
		db.collectionName.find({field: { $ne: value}});
	*/

	db.users.find({age: {$ne: 82}})

	 // $in operator
	/*
		- Allows us to find documents with specific match criteria one with one field using diffent values.
		- Syntax:
			db.collectionName.find({field: {$in: [valueA, valueB]}});
	*/

	db.users.find(
		{
			lastName:{
				$in: ['Hawking', 'Doe']
			}	
		}
	)

	db.users.find(
		{
			courses:{
				$in: ["HTML", "React"]
			}
		}
	)

// [SECTION] Logical Query Operators
// $or (1 true = true)	
/*
	- Allows us to find documents that match a single criteria from multiple provide search criteria
	- Syntax:
		db.collectionName.find({ $or: [{fieldA:valueA}, {fieldB:valueB}]});
*/


	db.users.find(
			{
				$or: [
					{firstName: "Neil"},
					{age: 21}
				]	
			}
	);

	// Additional example
	db.users.find(
		{
			$or:[
				{firstName: "Neil"},
				{age: {$gt: 25}}
			]
		}
	);



// $and (all should be satisfied)
/*
	- Allows us to find documents matching multiple criteria in a single field.
	- Syntax:
		db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: valueB}]});
*/


	db.users.find(
		{
			$and: [
				{age: {$ne:82}},
				{age: {$ne:76}}
			]	
		}
	)

	// Additional Example - multiple fields
	db.users.find(
		{
			$and:[
				{age: {$ne: 82}},
				{department: "Operations"}
			]
		}
	);


// [SECTION] Field Projection
// To help with the readability of the values returned, we can include/exclude fields from the response.

// Inclusion (1)
/*
	- Allows us to include/add specific fields only when retrieving a document.
	- The value provided is 1 to denote that the field is being included in the retrieval.
	- Syntax:
		db.collectionName.find({criteria}, {field: 1});
*/
db.users.find(
	{
		// criteria
		firstName: "Jane"	
	},
	{
		firstName:1,
		lastName:1,
		contact:1
	}
)


// Exclusion
/*
	- Allows us to exclude/remove specific fields only when retrieving documents.
	- The value provided is 0 to denote that fields is being excluded.
	- Syntax:
	 	db.users.find({criteria}, {field: 0});
*/
	

db.users.find(
	{
		// criteria
		firstName: "Jane"	
	},
	{
		contact: 0,
		department: 0
	}
)



// Not suggestable
db.users.find(
	{
		// criteria
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		department: 0
	}
)


// Supressing the ID field
	/*
		- When using field projection, field "inclusion" and "exclusion" may not be used at the same time.
		- Excluding the "_id" field is the only exception to this rule.
		- Syntax: 
			db.collectionName.find({critea}, {_id:0})

	*/

db.users.find(
	{
		// criteria
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
)


// [SECTION] Evaluation Query Operators
// $regex operator
/*
	- Allows us to find documents that match a specific string pattern using regular expressions.
	- Syntax:
		db.users.find({field: $regex: "pattern", $options: $optionValue});
*/


// Case sensitive
db.users.find(
	{
		firstName: {
			$regex: 'N'
		}
	}
)


// Not case sensitive
db.users.find(
	{
		firstName: {
			$regex: 'N', $option: '$i'
		}
	}
)